from ece650_server.apps.questions.models import Question
from django.http import JsonResponse, HttpResponse


def all_questions(request):
    question = Question()
    questions = question.get_all_questions()
    print(questions)
    #Error: questions is not JSON serializable
    return JsonResponse(questions, safe=False)

def questions_count(request):
    return HttpResponse( Question().get_questions_count() );

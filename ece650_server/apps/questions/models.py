from django.db import models
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from django.utils import timezone
from django.utils.html import strip_tags
from HTMLParser import HTMLParser


class Question(models.Model):
    question = RichTextField(_('question'), max_length=2048, default='')
    answer_a = models.CharField(_('answer_a'), max_length=150, default='')
    answer_b = models.CharField(_('answer_b'), max_length=150, default='')
    answer_c = models.CharField(_('answer_c'), max_length=150, default='')
    answer_d = models.CharField(_('answer_d'), max_length=150, default='')
    correct_answer = models.CharField(_('correct_answer'), max_length=150, default='')
    timestamp = models.DateTimeField(default=timezone.now)

    class Meta:
        verbose_name = _('question')
        verbose_name_plural = _('questions')

    def get_all_questions(self):
        questions = Question.objects.all()
        return questions

    def get_questions_count(self):
        return Question.objects.all().count()

    def save(self, *args, **kwargs):
        self.question = strip_tags(HTMLParser().unescape(self.question))
        super(self.__class__, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.question

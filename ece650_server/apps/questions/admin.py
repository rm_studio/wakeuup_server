from django.contrib import admin
from ece650_server.apps.questions.models import Question


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'question', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'correct_answer', 'timestamp')
    search_fields = ('id', 'question', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'correct_answer', 'timestamp')

admin.site.register(Question, QuestionAdmin)

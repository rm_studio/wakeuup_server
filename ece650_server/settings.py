# settings/base.py

"""
Django settings for insurance_log project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

from ece650_server.conf.base import *
from ece650_server.conf.local import *


from django.utils.encoding import force_unicode
from django.shortcuts import render
from django.contrib.admin.models import LogEntry
from django.contrib.contenttypes.models import ContentType
from .admin import *


def log_test(request):
    #LogAdmin.log_change( None, request, LogEntry, 'Try It!' )
    from django.contrib.admin.models import LogEntry, CHANGE  
    LogEntry.objects.log_action(  
        user_id=request.user.pk,
        content_type_id=ContentType.objects.get_for_model(request.user).pk,
        object_id=LogEntry.pk,
        object_repr=force_unicode(request.user),
        action_flag=CHANGE,
        change_message='Hello, world!'
    )  
    pass

# Create your views here.

from django.db import models
from django.contrib import admin
from .models import *
from mptt.admin import MPTTModelAdmin


class TeamAdmin(MPTTModelAdmin):
    pass


admin.site.register(Team, TeamAdmin)

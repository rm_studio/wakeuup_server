from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .core.log.views import *
from .core.gallery.views import *
from .apps.questions.models import *
from rest_framework import routers, serializers, viewsets

# Question serializers define the API representation.
class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question
        fields = ('id', 'question', 'answer_a', 'answer_b', 'answer_c', 'answer_d', 'correct_answer', 'timestamp')

class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

router = routers.DefaultRouter()
router.register(r'questions', QuestionViewSet)


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ece650_server.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),    
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^log-test/', log_test),

    url(r'^question/all$', 'ece650_server.apps.questions.views.all_questions', name='all_questions'),
    url(r'^question/count$', 'ece650_server.apps.questions.views.questions_count', name='questions_count'),

    # rest api.
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)

# Append
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)    
urlpatterns += staticfiles_urlpatterns()
